package com.rapidsoft.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assertions {
	
	WebDriver driver;

	@After
	public void stopWebDriver() {
		System.out.println("In @ After");
		driver.close();
	}
	
	@Before
	public void startWebDriver() {
		driver = new ChromeDriver();
		
		driver.get("http://newtours.demoaut.com/");
		driver.manage().window().maximize();
		System.out.println("In @ Before");
	}

	@Ignore @Test
	public void testAWebDriverAPIs() {
        
		String pageTitle = driver.getTitle();
		System.out.println("pageTile = " + pageTitle);
		if (pageTitle.equals("Welcome")) {
			System.out.println("You are on the right page");
		}
		Assert.assertTrue("Assertion to verify page title", pageTitle.contains("Welcome"));
        Assert.assertEquals(pageTitle, "Welcome: Mercury Tours", "Comparing page titles");
        
		driver.findElement(By.name("userName")).sendKeys("demo");
		driver.findElement(By.name("password")).sendKeys("demo");
		driver.findElement(By.name("login")).submit();
		
	}


	


	

}
