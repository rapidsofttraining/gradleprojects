package com.rapidsoft.tests;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestNGAssertions {
	
	WebDriver driver;

	@AfterTest
	public void stopWebDriver() {
		System.out.println("In @ After");
		driver.close();
	}
	
	@Ignore@BeforeTest
	public void startWebDriver() {
		driver = new ChromeDriver();
		
		driver.get("http://newtours.demoaut.com/");
		driver.manage().window().maximize();
		System.out.println("In @ Before");
	}

	@Ignore@Test 
	public void testAWebDriverAPIs() {
        
		String pageTitle = driver.getTitle();
		System.out.println("pageTile = " + pageTitle);
		if (pageTitle.equals("Welcome")) {
			System.out.println("You are on the right page");
		}
//  	Assert.assertTrue( pageTitle.contains("Welcome1"), "Assertion to verify page title");
//      Assert.assertEquals(pageTitle, "Welcome: Mercury Tours", "Comparing page titles");
		
		SoftAssert sAssert = new SoftAssert();
		System.out.println("trying soft assert...");
		sAssert.assertTrue(pageTitle.contains("Welcome"), "Assertion to verify page title");

		driver.findElement(By.name("userName")).sendKeys("demo");
		driver.findElement(By.name("password")).sendKeys("demo");
		driver.findElement(By.name("login")).submit();
		System.out.println("after login ...");
		sAssert.assertAll();
		
		
	}


	


	

}
