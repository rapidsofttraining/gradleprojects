package stepdefs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import pageobjects.BasePage;

public class cucumberhooks extends BasePage {

	@Before
	public void InitializeTest() {

		System.out.println("--- In HookOpening the browser : Chrome");
		//System.setProperty("webdriver.chrome.driver", "C:\\workspace\\SeleniumWithCucucumber\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
		WebDriver wd = new ChromeDriver();
		setDriver(wd);

	}

	@After
	public void TearDownTest(Scenario scenario) {
		// if (scenario.isFailed()) {
		// System.out.println(scenario.getName());
		// }
		System.out.println("--- In Hook closing the browser : Chrome");
		// baseUtil.Driver.close();
		getDriver().close();
	}

}
