package stepdefs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.BasePage;
import pageobjects.SignupPageObject;

public class EnrollStudentStepDefs extends BasePage{
	WebDriver driver;
	SignupPageObject signpage;

	public EnrollStudentStepDefs() {
//		System.setProperty("webdriver.chrome.driver", "C:\\workspace\\SeleniumWithCucucumber\\chromedriver.exe");
//		driver = new ChromeDriver();
		signpage = new SignupPageObject(getDriver());
	}

	@Given("^acess to the website$")
	public void acess_to_the_website() throws Throwable {
		System.out.println("acess to the website");
		signpage.navigateToGoogleHomePage();

	}

	@When("^I enter username$")
	public void i_enter_username() throws Throwable {
		System.out.println("I enter username");
		signpage.searchOnGooglePage("Anand");
	}

	@When("^I enter password$")
	public void i_enter_password() throws Throwable {
		System.out.println("I enter password");
	}

	@Then("^I should be able to login and see message \"([^\"]*)\"$")
	public void i_should_be_able_to_login_and_see_message(String arg1) throws Throwable {

		System.out.println("coming from feature file = " + arg1);

	}

}
