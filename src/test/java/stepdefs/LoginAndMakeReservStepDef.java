/**
 * 
 */
package stepdefs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.BasePage;
import pageobjects.FlightFinderPage;
import pageobjects.LoginPageObject;
/**
 * @author RapidSoft
 *
*/

public class LoginAndMakeReservStepDef extends BasePage {
	//WebDriver driver; // this is not required 
    LoginPageObject loginPage;
    FlightFinderPage flightFinderpage;
	@Given("^access to the website in int$")
	public void access_to_the_website_in_int() throws Throwable {
//		System.out.println("---access to the website");
//		//System.setProperty("webdriver.chrome.driver", "C:\\workspace\\SeleniumWithCucucumber\\chromedriver.exe");
//		System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
//		driver = new ChromeDriver();
		getDriver().get("http://newtours.demoaut.com/");
		loginPage = new LoginPageObject(getDriver());
		flightFinderpage = new FlightFinderPage(getDriver());
	}

	@When("^I enter user and password$")
	public void i_enter_user_and_password() throws Throwable {
		System.out.println("---i_enter_user_and_password");
		loginPage.enterUserName("demo");
		loginPage.enterPassword("demo");
		
		

// before moving  resource locator and action into PageOject (LoginPageObject.java)
//		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys("demo");
//		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("demo");
//		driver.findElement(By.xpath("//input[@name='login']")).submit();
	}

	@When("^select account type$")
	public void select_account_type() throws Throwable {
	 System.out.println("I am in account type step ");
	 loginPage.selectAccountType("Personal");
	 
	}
	
	
	@When("^after login search for flights$")
	public void after_login_search_for_flights() throws Throwable {
		loginPage.submitLogin();
	}

	@When("^select a flight$")
	public void select_a_flight() throws Throwable {
		System.out.println("select a flight and continue");
		flightFinderpage.continuewithreservation();
		
	}

	@When("^make reservation$")
	public void make_reservation() throws Throwable {
	}

	@Then("^confirm the reservation$")
	public void confirm_the_reservation() throws Throwable {
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
