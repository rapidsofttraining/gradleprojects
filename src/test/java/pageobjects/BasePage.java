package pageobjects;

import org.openqa.selenium.WebDriver;

public class BasePage {
	private static WebDriver Driver;

	public void setDriver(WebDriver wd) {
		
		Driver = wd;
		System.out.println("setting the driver");
	}

	public WebDriver getDriver() {
		return Driver;
	}

}
