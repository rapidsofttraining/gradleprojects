package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObject extends BasePage {

	// webelement actions declared here
	By userName = By.xpath("//input[@name='userName']");
	By passwordby = By.xpath("//input[@name='password']");
	By selectAccountType = By.xpath("//input[@name='password']");
	By submit = By.xpath("//input[@name='login']");

	WebDriver driver;

	public LoginPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void enterUserName(String username) {
		driver.findElement(userName).sendKeys("demo");
		System.out.println("enterUserName");
	}

	public void enterPassword(String password) {
		driver.findElement(passwordby).sendKeys("demo");
		System.out.println("enterPassword");
	}
	
	public void selectAccountType(String accType) {
		driver.findElement(selectAccountType).sendKeys(accType);
		System.out.println("I have selected account type");
	}
	
	
	

	public void submitLogin()

	{
		// Before seperation of resouce locator and action
		// driver.findElement(By.xpath("//input[@name='login']")).submit();
		// After seperation of resouce locator and action
		driver.findElement(submit).submit();
	}
	
	

}
