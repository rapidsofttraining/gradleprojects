package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FlightFinderPage extends BasePage {
	WebDriver driver;
	By continueButton = By.xpath("//input[@name='findFlights']");

	public FlightFinderPage(WebDriver driver) {
		this.driver = driver;
	}

	public void continuewithreservation() throws InterruptedException {
		System.out.println(" continuewithreservation--");
		driver.findElement(continueButton).submit();
	}
}
