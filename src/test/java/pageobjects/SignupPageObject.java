package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignupPageObject {
    WebDriver driver;
	public SignupPageObject(WebDriver driver) {
		this.driver = driver;
	}
	// By username = By.xpath("//xyz");
	// @FindBy(xpath="//abc")
	// WebElement searchbox;

	@FindBy(how = How.ID, using = "lst-ib")
	public static WebElement searchBox;

	public void navigateToGoogleHomePage() {
	//	BaseUtil.Driver.get("https://www.google.com");
		driver.get("https://www.google.com");		
	}

	public void searchOnGooglePage(String searchTerm) {
		System.out.println("searchterm is "  + searchTerm);
//		searchBox.sendKeys(searchTerm);
		driver.findElement(By.id("lst-ib")).sendKeys(searchTerm);

	}	
	
}
