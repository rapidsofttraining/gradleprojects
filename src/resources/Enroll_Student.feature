Feature: Enroll a student

Scenario Outline: Enroll a new student
Given acess to the website
When I enter <username> 
And I enter <password>
Then I should be able to login and see message <message> 

Examples:
    | username  |password | message   |
    | demo      |  demo   | welcome   |
    
